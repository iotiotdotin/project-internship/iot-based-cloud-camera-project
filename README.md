# IoT Based Cloud Camera Project

Hello World! Are you interested in working on an Industry ready project which would help you get out of the comfort zone of college projects and really push your limits? You have come to the right place. We are going to be working on a security camera that gets connected to the cloud via QR Code and starts recording audio and video through the camera when motion or voice is detected. So what do you say we give a shot at making our own version of an Amazon Cloud Camera? This will be a safe space where we can make mistakes and most importantly learn. Positive feedback or constructrive suggestions are always welcome but let us keep it friendly, alright? Significant contributions will be recognised sufficiently;) What are you waiting for? Let's get started!

The objective of the project is to ensure that homes and other facilities are prevented from being broken into. It also helps the owner to keep track of visitors to their particular abode.

🦾 Why should you be a part of this project?
1. Security-related products have a great demand in the market.
2. Opportunity to work on an Industry related product
3. Highly scalable as it is relevant from households to Big factories and Companies

🦾 Who should take up this project?  
1. Motivated and determined to get the most out of this experience
2. Those who want to make an actual industrial product
3. Willing to learn and implement along the way
4. Self-motivated and ready to bring their best for the growth of the project

🦾 Skills appreciated
1. Python programming
2. Android App/Web development
3. Familiar with AWS Cloud Services
4. Basic communication and documentation skills

**Even if you do not have the mentioned skills but are willing to pick them up along the way, do not hesitate to start contributing**

🦾Enthusiasm and the willingness to have fun while learning is welcome😊

Fundamental Steps:  
1. Fork this Project  
2. Look at assigned tasks in the issues
3. Complete the Tasks and commit your submissions to your repository
4. Comment with the link to your completed submission under the comments of each issue  
5. Pat yourself in the back for bring an amazing contributor  
*Similar to the skilling modules you finished*

We will be using Shunya Interfaces to **simulate** communication between devices(Sensors & Actuators), development boards and the cloud. Take a look at this [link](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/SI/README) when you cross the bridge.
